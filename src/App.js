import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>My first App on vercel</h1>
      </header>
      <main>
        <p>
           I'm Luigi Llerena 
        </p>
      </main>
    </div>
  );
}

export default App;
